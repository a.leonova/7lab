import java.io.IOException;

public class ProviderForServer implements Connection {

    private DataForwardsPair connection;

    public ProviderForServer(DataForwardsPair connection) {
        this.connection = connection;
    }

    @Override
    public void read() throws IOException {
        connection.getFrom().read();
    }

    @Override
    public void write() throws IOException {
        connection.getTo().write();
    }
}
