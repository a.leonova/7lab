public class DataForwardsPair {
    private DataForwarder from;
    private DataForwarder to;

    public DataForwardsPair(DataForwarder from, DataForwarder to) {
        this.from = from;
        this.to = to;
    }

    public DataForwarder getFrom() {
        return from;
    }

    public DataForwarder getTo() {
        return to;
    }
}
