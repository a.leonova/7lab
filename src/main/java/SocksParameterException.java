public class SocksParameterException extends Exception {
    public SocksParameterException() {
    }

    public SocksParameterException(String message) {
        super(message);
    }
}
