import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.*;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.util.Arrays;

public class DNSResolver implements Connection {

    private static final Logger LOGGER = LoggerFactory.getLogger(DNSResolver.class);
    private final DatagramChannel datagramChannel;
    private final SelectionKey selectionKey;
    private DomainNamesCache domainNamesCache;

    public DNSResolver(Selector selector, DomainNamesCache domainNamesCache) throws IOException {
        try {
            datagramChannel = DatagramChannel.open();
        } catch (IOException e) {
            LOGGER.error("cannot create datagramChannel");
            throw e;
        }

        this.domainNamesCache = domainNamesCache;
        String[] servers = ResolverConfig.getCurrentConfig().servers();
        LOGGER.debug("Found recursive DNS servers: " + Arrays.toString(servers));
        datagramChannel.configureBlocking(false);
        datagramChannel.connect(new InetSocketAddress(servers[servers.length - 1], 53));
        selectionKey = datagramChannel.register(selector, 0);
        selectionKey.attach(this);
    }

    public void write(String domainName) throws IOException {
        LOGGER.debug("GOT NEW REQUEST: ", domainName);
        Name name = Name.fromString(domainName);
        Record question = Record.newRecord(name, Type.A, DClass.IN);
        Message query = Message.newQuery(question);
        byte[] requestBytes = query.toWire();
        datagramChannel.write(ByteBuffer.wrap(requestBytes));
        selectionKey.interestOps(SelectionKey.OP_READ);
    }

    public void write(){}

    public void read() throws IOException, CreateConnectionException {
        LOGGER.debug("DNS_READ");
        byte[] bytes = new byte[16 * 1024];
        datagramChannel.read(ByteBuffer.wrap(bytes));
        Message message = new Message(bytes);
        Record[] records = message.getSectionArray(Section.ANSWER);
        for (Record r : records) {
            LOGGER.debug(records.toString());
            if (r.getType() == Type.A) {
                ARecord aRecord = (ARecord) r;
                byte[] ipAddr = aRecord.getAddress().getAddress();
                String name = aRecord.getName().toString();
                domainNamesCache.putInetAddress(name, InetAddress.getByAddress(ipAddr));
            }
        }

        for(Record r :records){
            if(r.getType() == Type.CNAME){
                domainNamesCache.addAlias(r.getName().toString(),((CNAMERecord)r).getAlias().toString());
            }
        }
    }
}
