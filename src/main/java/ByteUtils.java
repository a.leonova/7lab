import java.nio.ByteBuffer;

public class ByteUtils {

    public static byte[] intTo2Bytes(int value) {
        ByteBuffer bb = ByteBuffer.allocate(Integer.BYTES);
        bb.putInt(value);
        byte[] res = {bb.get(2), bb.get(3)};
        return res;
    }

    public static int BytesToInt(byte[] bytes) {
        byte[] value = {0,0, bytes[0], bytes[1]};
        ByteBuffer byteBuffer = ByteBuffer.wrap(value);
        return byteBuffer.getInt();
    }
}
