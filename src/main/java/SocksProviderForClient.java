import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public class SocksProviderForClient implements Connection {

    private static final Logger LOGGER = LoggerFactory.getLogger(SocksProviderForClient.class);

    private State state = State.READY_FOR_GREETING;
    private SelectionKey key;
    private Selector selector;
    private ByteBuffer answer = ByteBuffer.allocate(5);

    private ByteBuffer buffer = ByteBuffer.allocate(300);
    private DataForwardsPair connection;
    private DomainNamesCache domainNamesCache;
    private DNSResolver resolver;
    private int port;

    public SocksProviderForClient(SelectionKey key, DomainNamesCache domainNamesCache, DNSResolver dnsResolver, Selector selector) {
        this.key = key;
        this.domainNamesCache = domainNamesCache;
        this.resolver = dnsResolver;
        this.selector = selector;
    }

    public void read() throws IOException, IllegalArgumentException, CreateConnectionException {
        LOGGER.debug("Reading from {} key. In state {}", key, state);
        switch (state){
            case READY_FOR_MESSAGE:
                connection.getFrom().read();
                break;
            case READY_FOR_GREETING:
                readGreeting();
                state = State.READY_FOR_GREETING_ANSWER;
                key.interestOps(key.interestOps() & ~SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                break;
            case READY_FOR_REQUEST:
                getRequest();
                if(state != State.WAITING_FOR_DNS_RESOLVING){
                    state = State.READY_FOR_REQUEST_ANSWER;
                    key.interestOps(key.interestOps() & ~SelectionKey.OP_READ | SelectionKey.OP_WRITE);
                }
                break;

                default: key.cancel();
        }
    }


    public void write() throws IOException {
        LOGGER.debug("Writing from {} key. In state {}", key, state);
        switch (state){
            case READY_FOR_MESSAGE:
                LOGGER.debug("WRITE MESSAGE: key - {}", key);
                connection.getTo().write();
                break;
            case READY_FOR_REQUEST_ANSWER:
                sendAnswer();
                key.interestOps(key.interestOps() & ~SelectionKey.OP_WRITE | SelectionKey.OP_READ);
                state = State.READY_FOR_MESSAGE;
                break;
            case READY_FOR_GREETING_ANSWER:
                sendAnswer();
                key.interestOps(key.interestOps() & ~SelectionKey.OP_WRITE | SelectionKey.OP_READ);
                state = State.READY_FOR_REQUEST;
                break;

                default: key.cancel();
        }
    }

    public void notifyAddress(InetAddress inetAddress) throws CreateConnectionException {
        InetSocketAddress remoteHostAddress = new InetSocketAddress(inetAddress, port);
        createNewConnection(remoteHostAddress);
        state = State.READY_FOR_REQUEST_ANSWER;
        key.interestOps(SelectionKey.OP_WRITE);
    }


    private void sendAnswer() throws IOException {
        answer.flip();
        int r =((SocketChannel)key.channel()).write(answer);
        LOGGER.debug("key - {}, written = {}", key, r);
    }

    private void readGreeting() throws IOException {
        LOGGER.debug("Reading greeting: key - {}", key);
        SocketChannel channel = (SocketChannel) key.channel();
        int read = channel.read(buffer);
        if(read == -1){
            LOGGER.error("1NO DATA IN CONNECTION");
            throw new IllegalArgumentException("No data in greeting");
        }
        if(buffer.get(0) != Constants.SOCKS_VERSION_5){
            LOGGER.error("BAD SOCKS VERSION: key - {}, version - {}", key, buffer.get(0));
            throw new IllegalArgumentException("Not SOCKS5");
        }
        int count = buffer.get(1);
        LOGGER.debug("Methods count: key - {}, count - {}", key, count);
        for (int i = 0; i < count; ++i){
            LOGGER.debug("Method: key - {}, method - {}", key, buffer.get(2 + i));
            if(buffer.get(2 + i) == Constants.NO_AUTH){
                fillAnswerGreeting(Constants.NO_AUTH);
                return;
            }
        }
        LOGGER.info("No necessary method (0x00): key - {}", key);
        fillAnswerGreeting(Constants.NO_METHOD);
        return;
    }

    private void fillAnswerGreeting(byte state){
        answer = ByteBuffer.allocate(2);
        answer.put(Constants.SOCKS_VERSION_5);
        answer.put(state);
    }

    private void getRequest() throws IOException, CreateConnectionException {
        LOGGER.debug("Reading request parameters: key - {}", key);
        SocketChannel channel = (SocketChannel) key.channel();
        buffer.clear();

        int read = channel.read(buffer);
        if(read == -1){
            LOGGER.error("NO DATA IN CONNECTION");
            throw new IllegalArgumentException("No data in greeting");
        }
        if(buffer.get(0) != Constants.SOCKS_VERSION_5){
            LOGGER.error("BAD SOCKS VERSION: key - {}, version - {}", key, buffer.get(0));
            throw new IllegalArgumentException("NOT SOCKS5");
        }
        if(buffer.get(1) != Constants.TCP_IP_STREAM_CONNECTION){
            LOGGER.error("BAD CONNECTION TYPE: key - {}, type - {}", key, buffer.get(1));
            fillRequestAnswer(Constants.COMMAND_NOT_SUPPORTED_STATUS, ByteUtils.intTo2Bytes(0), Constants.zeroIP);
            return;
        }
        int count;

        InetAddress address;
        if (buffer.get(3) == 0x01){
            byte[] addressBytes = {buffer.get(4), buffer.get(5), buffer.get(6), buffer.get(7)};
            port = getPort(8);
            address = InetAddress.getByAddress(addressBytes);
        }
        else if(buffer.get(3) == Constants.DOMAIN_NAME){
            count = buffer.get(4);
            String domainName = getDomainName(count);
            port = getPort(5 + count);
            byte[] domainNameBytes = domainName.getBytes();
            String fullQualifiedDomainName = new String(domainNameBytes) +
                    ( domainNameBytes[domainNameBytes.length - 1] == '.' ? "" : ".");
            address = domainNamesCache.getInetAddress(fullQualifiedDomainName);
            if(address == null){
                LOGGER.debug("got fullQualifiedDomainName: " + fullQualifiedDomainName);
                domainNamesCache.addRequiredInResolving(fullQualifiedDomainName, this);
                resolver.write(fullQualifiedDomainName);
                this.state = State.WAITING_FOR_DNS_RESOLVING;
                key.interestOps(0);
                return;
            }
        }
        else {
            LOGGER.error("Ipv6 type: key - {}, address type - {}", key, buffer.get(3));
            throw new IllegalArgumentException("IPv6 tyoe");
        }
        LOGGER.debug("key - {}, port - {}", key, port);
        InetSocketAddress remoteHostAddress = new InetSocketAddress(address, port);
        createNewConnection(remoteHostAddress);
    }

    private int getPort(int offset){
        byte[] portInBytes = {buffer.get(offset), buffer.get(offset + 1)};
        return ByteUtils.BytesToInt(portInBytes);
    }

    private String getDomainName(int count) {
        StringBuilder domainName = new StringBuilder(count);
        for(int i = 0 ; i < count; ++i){
            domainName.append((char)buffer.get(5 + i));
        }
        LOGGER.debug("Key - {}, domain name - {}", key, domainName);
        return domainName.toString();
    }

    private void createNewConnection(InetSocketAddress remoteHostAddress) throws CreateConnectionException {
        try{
            LOGGER.debug("Host - {}, port - {}", remoteHostAddress.getAddress().getAddress(), remoteHostAddress.getPort());
            SocketChannel remoteHostChannel = SocketChannel.open(remoteHostAddress);
            remoteHostChannel.configureBlocking(false);
            SelectionKey selectionKeyFromRemoteHost = remoteHostChannel.register(selector, SelectionKey.OP_READ);
            LOGGER.debug("ADDED Server key - {}", selectionKeyFromRemoteHost);
            fillRequestAnswer(Constants.REQUEST_GRANTED, ByteUtils.intTo2Bytes(remoteHostChannel.socket().getLocalPort()),
                    remoteHostAddress.getAddress().getAddress());

            DataForwarder fromServerToClient = new DataForwarder(selectionKeyFromRemoteHost, key);
            DataForwarder fromClientToServer = new DataForwarder(key, selectionKeyFromRemoteHost);

            ProviderForServer providerForRemoteServer = new ProviderForServer(new DataForwardsPair(fromServerToClient, fromClientToServer));
            selectionKeyFromRemoteHost.attach(providerForRemoteServer);
            connection = new DataForwardsPair(fromClientToServer, fromServerToClient);
        }
        catch (IOException e){
            LOGGER.error("Exception: ", e);
            key.cancel();
            throw new CreateConnectionException();
        }
    }

    private void fillRequestAnswer(byte status, byte[] port, byte[] ip){
        answer = ByteBuffer.allocate(10);
        answer.put(Constants.SOCKS_VERSION_5);
        answer.put(status);
        answer.put((byte) 0x00);
        answer.put(Constants.IPv4);
        answer.put(ip[0]);
        answer.put(ip[1]);
        answer.put(ip[2]);
        answer.put(ip[3]);
        answer.put(port[0]);
        answer.put(port[1]);
    }
}
