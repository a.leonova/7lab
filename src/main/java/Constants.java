public class Constants {
    public static byte SOCKS_VERSION_5 = 0x05;
    public static byte NO_AUTH = 0x00;
    public static byte TCP_IP_STREAM_CONNECTION = 0x01;
    public static byte COMMAND_NOT_SUPPORTED_STATUS = 0x07;
    public static byte REQUEST_GRANTED = 0x00;
    public static byte IPv4 = 0x01;
    public static byte DOMAIN_NAME = 0x03;
    public static byte[] zeroIP = {0, 0, 0, 0};
    public static byte NO_METHOD = (byte)0xFF;
}
