import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xbill.DNS.Message;

import java.io.IOException;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;

public class DataForwarder {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataForwarder.class);
    private static final int SIZE = 16 * 1024;

    private SelectionKey in;
    private SelectionKey out;
    private boolean closeConnection = false;
    private ByteBuffer buffer = ByteBuffer.allocate(SIZE);

    public DataForwarder(SelectionKey in, SelectionKey out) {
        this.in = in;
        this.out = out;
    }

    public void read() {
        try{
            LOGGER.debug("{} : READING in DATAFORWARDER", this);
            SocketChannel channel = (SocketChannel) in.channel();
            int readBytes = channel.read(buffer);
            LOGGER.debug("{}: READ {} bytes", this, readBytes);
            if(!buffer.hasRemaining()){
                LOGGER.debug("{}: NO MORE READING INFO", this);
                in.interestOps(in.interestOps() & ~SelectionKey.OP_READ);
            }
            if (readBytes == -1){
                LOGGER.debug("{}: NO MORE READING INFO. CHANGE STATE", this);
                closeConnection = true;
                in.interestOps(in.interestOps() & ~SelectionKey.OP_READ);
            }
            if(buffer.position() != 0 || closeConnection){
                out.interestOps(out.interestOps() | SelectionKey.OP_WRITE);
            }
        } catch (IOException e){
            LOGGER.error("{} CANT READ: ", this, e);
            out.interestOps(0);
            in.interestOps(0);
            closeConnection = true;
        }
    }

    public void write() {
        try{
            LOGGER.debug("{}: WRITING in DATAFORWARDER", this);
            SocketChannel channel = (SocketChannel) out.channel();
            buffer.flip();
            int wroteBytes = channel.write(buffer);
            LOGGER.debug("{}: WRITE {} bytes", this, wroteBytes);
            if(!buffer.hasRemaining()){
                LOGGER.debug("{}: NO REMAINING IN WRITE", this);
                if(closeConnection){
                    LOGGER.debug("{}: CLOSE CONNECTION: in {}, out {}",this, in, out);
                    channel.shutdownOutput();
                }
                out.interestOps(out.interestOps() & ~ SelectionKey.OP_WRITE);
            }
            buffer.compact();
            if(buffer.hasRemaining() && !closeConnection){
                LOGGER.debug("{}: INTEREST IN READ", this);
                in.interestOps(in.interestOps() | SelectionKey.OP_READ);
            }
        }catch (IOException e){
            LOGGER.error("{} CANT WRITE: ", this, e);
            in.interestOps(0);
            out.interestOps(0);
            closeConnection = true;
        }
    }

}
