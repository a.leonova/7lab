public enum State {
    //for read
    READY_FOR_GREETING,
    READY_FOR_REQUEST,
    READY_FOR_MESSAGE,
    //for write
    READY_FOR_GREETING_ANSWER,
    READY_FOR_REQUEST_ANSWER,
    //for dns resolving
    WAITING_FOR_DNS_RESOLVING
}
