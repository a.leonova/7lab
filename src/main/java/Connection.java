import java.io.IOException;

public interface Connection {

    void read() throws IOException, CreateConnectionException;
    void write() throws IOException;
}
