import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class DomainNamesCache {
    private HashMap<String, InetAddress> nameToIP = new HashMap<>();
    private HashMap<String, Set<SocksProviderForClient>> requiredInResolving = new HashMap<>();

    public InetAddress getInetAddress(String name){
        return nameToIP.get(name);
    }

    public void putInetAddress(String name, InetAddress address) throws CreateConnectionException {
        nameToIP.put(name, address);

        Set<SocksProviderForClient> providers = requiredInResolving.get(name);

        if(providers != null){
            for (SocksProviderForClient socksProviderForClient : providers) {
                socksProviderForClient.notifyAddress(address);
            }
            requiredInResolving.remove(name);
        }
    }

    public Set<String> getResolvingDomain(){
        return requiredInResolving.keySet();
    }

    public void addRequiredInResolving(String name, SocksProviderForClient socksProviderForClient){
        requiredInResolving.computeIfAbsent(name, k -> new HashSet<>());
        requiredInResolving.get(name).add(socksProviderForClient);
    }

    public void addAlias(String domainName, String alias) throws CreateConnectionException {
        InetAddress adr = nameToIP.get(alias);
        if(adr != null){
            putInetAddress(domainName, adr);
        }
    }
}
