import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;

public class Main {

    private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

    private Selector selector = Selector.open();
    private ServerSocketChannel serverSocket = ServerSocketChannel.open();
    private DomainNamesCache domainNamesCache = new DomainNamesCache();
    private DNSResolver dnsResolver = new DNSResolver(selector, domainNamesCache);

    public Main() throws IOException {
    }

    public static void main(String[] args) throws IOException {
        InetSocketAddress mySocket;
        int port;
        try {
            port = Integer.parseInt(args[0]);
            mySocket = new InetSocketAddress("localhost", port);
        } catch (NumberFormatException e) {
            System.err.println("Bad port: " + e.getMessage());
            return;
        } catch (IllegalArgumentException e) {
            System.err.println("Bad arguments: " + e.getMessage());
            return;
        }
        new Main().startServer(mySocket);
    }

    public void startServer(InetSocketAddress mySocket) {
        try {
            serverSocket.bind(mySocket);
            serverSocket.configureBlocking(false);
            serverSocket.register(selector, SelectionKey.OP_ACCEPT);
        }
        catch (IOException e){
            LOGGER.error("Exception. Cannot start program: ", e);
        }

        while (true) {
            try{
                if (selector.select() <= 0)
                    continue;
            } catch (IOException e){
                LOGGER.error("Exception: ", e);
            }
            Set<SelectionKey> selectedKeys = selector.selectedKeys();
            Iterator<SelectionKey> iter = selectedKeys.iterator();
            while (iter.hasNext()) {
                SelectionKey key = iter.next();
                try{
                    if (key.isAcceptable()) {
                        register();
                    }
                    if(key.isReadable()){
                        ((Connection)key.attachment()).read();
                    }
                    if(key.isWritable()){
                        ((Connection)key.attachment()).write();
                    }
                } catch (IOException | IllegalArgumentException e){
                    LOGGER.error("Exception: ", e);
                    key.cancel();
                    continue;
                } catch (CreateConnectionException e){
                    LOGGER.error("Couldn't create connection");
                    continue;
                }
            }
            selectedKeys.clear();
        }
    }

    public void register() throws IOException {
        SocketChannel client = serverSocket.accept();
        client.configureBlocking(false);
        SelectionKey selectionKeyFromClient = client.register(selector, SelectionKey.OP_READ);

        LOGGER.debug("ADDED NEW KEY - {}", selectionKeyFromClient);
        SocksProviderForClient socksForClient = new SocksProviderForClient(selectionKeyFromClient, domainNamesCache, dnsResolver, selector);
        selectionKeyFromClient.attach(socksForClient);
    }
}
